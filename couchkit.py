#!/usr/bin/python
import couchdbkit

#http://127.0.0.1:6984/_utils/

def main():
        dburl  = 'http://admin:priwur@127.0.0.1:6984/'
	simple(dburl)


def simple(dburl):
	#http://docs.cloudant.com/using-cloudant-with/python.html
	
	server = couchdbkit.Server( dburl )
	db     = server.get_or_create_db( 'posts' )

	db.save_doc({
    		'author': 'Mike Broberg',
		'content': "In my younger and more vulnerable years, my father told me, 'Son, turn that racket down.'"
	})

	db.save_doc(
		{
			'chrom00': {
					1024: {
						'ref': 'A',
						'tgt': 'T',
						'nfo': {
							'q': 100,
							'v': 200,
							'x': [300,400,500]
						}
					}
				}
		}
	)

	db.save_doc(
		{
			'chrom01': {
					1025: {
						'ref': 'C',
						'tgt': 'G',
						'nfo': {
							'q': 600,
							'v': 700,
							'x': [800,900,1000]
						}
					}
				}
		}
	)

	# save lots of docs :D
	#docs = [{...}, {...}, {...}]
	#db.bulk_save(docs)


	# associate posts with a given database object
	Post.set_db(db)
	new_post = Post(
  		author="Mike Broberg",
  		content="In his spare time, Mike Broberg enjoys barbecuing meats of various origins."
	)

	# save the post to its associated database
	new_post.save()


	# print all docs in the database
	for doc in db.view('_all_docs'):
	  	print 'ALL', doc

	# or, do the same on a pre-defined index
	# where 'DDOC/INDEX' maps to '_design/DDOC/_view/INDEX'
	# in the HTTP API
#	for doc in db.view('DDOC/INDEX'):
#  		print doc

class Post(couchdbkit.Document):
	author = couchdbkit.StringProperty()
	content = couchdbkit.StringProperty()


if __name__ == "__main__":
	main()
