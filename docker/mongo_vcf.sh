#!/bin/bash

FUNC=$1
shift
PAR=$@
echo PAR $PAR

/home/user/mongo_vcf.py $FUNC \
-s $MONGOSERVER_PORT_27017_TCP_ADDR \
-p $MONGOSERVER_PORT_27017_TCP_PORT \
$PAR
