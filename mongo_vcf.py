#!/usr/bin/python
#http://api.mongodb.org/python/current/tutorial.html
import sys
import os
import pprint
from itertools import izip
import json
import urllib
import argparse
import time

from pymongo       import MongoClient
from pymongo       import ASCENDING, DESCENDING
from pymongo       import errors
from bson.objectid import ObjectId
from bson          import json_util

import vcf

import datetime


__version__ = "2014_05_16_00_43"

print 'v', __version__

pp = pprint.PrettyPrinter(indent=1)

colnames        = ['CHROM', 'POS', 'ID', 'REF', 'ALT', 'QUAL', 'FILTER', 'INFO', 'FORMAT' ]

nestedDb        = True
nestedDb        = False
debug           = 2 # -1 no; > 1 = delete database, number of samples to read
debug           = 100 # -1 no; > 1 = delete database, number of samples to read
debug           = -1 # -1 no; > 1 = delete database, number of samples to read
read_only       = -1
dump_file       = False
dump_file       = True
print_every     = 10000
read_only       = 25000

if debug > -1:
    read_only = debug

#MAX CONCURRENCY = 7
#MAX SPEED EACH  = 800 REC/SEC
#MAX SPEED TOTAL = 7 * 800 = 5600 REC/SEC
#TIME PER FILE   = 3000 SEC = 50 MIN
#TIME PER 85     = 3000 SEC * ( 85 / 7 ) = 36428 SEC = 607 MIN = 10 HOURS


#MAX UPDATE: 5 * 430 = 2150 RECS/S 10 * 230 = 2300 RECS/S
#MAX INSERT: 5 * 500 = 2500 RECS/S 10 * 300 = 3000 RECS/S
#
#docker run -i -t --rm -v $PWD/../data2/:/home/user/data:rw --name="sauloal_mongoapistatsvcf" --link sauloal_mongoapistats:mongoserver sauloal/mongoapistatsvcf file_list -d vcf
#docker run -i -t --rm -v $PWD/../data2/:/home/user/data:rw --name="sauloal_mongoapistatsvcf" --link sauloal_mongoapistats:mongoserver sauloal/mongoapistatsvcf command -d vcf compact
#
#alias vcfmongo="docker run -i -t --rm -v $PWD/../data2/:/home/user/data:rw --name='sauloal_mongoapistatsvcfdev' --link sauloal_mongoapistats:mongoserver sauloal/mongoapistatsvcfdev"
#vcfmongo db_del -d vcf -y; vcfmongo db_add -d vcf; find data/ -regex '.*RF_0[0|1].+\.vcf' | sort | xargs -P 7 -n 1 docker run -i -t --rm -v $PWD/data/:/home/user/data:rw --link sauloal_mongoapistats:mongoserver sauloal/mongoapistatsvcfdev file_add -d vcf; vcfmongo index_add -d vcf; vcfmongo command -d vcf compact
#vcfmongo db_del -d vcf -y; vcfmongo db_add -d vcf; find data/ -regex '.*.vcf' | sort | xargs -P 7 -n 1 docker run -i -t --rm -v $PWD/data/:/home/user/data:rw --link sauloal_mongoapistats:mongoserver sauloal/mongoapistatsvcfdev file_add -d vcf; vcfmongo index_add -d vcf; vcfmongo command -d vcf compact
#
#
#./mongo_vcf.py db_del -s localhost -p 27017 -d vcf -y; ./mongo_vcf.py db_add -s localhost -p 27017 -d vcf; find ../data/ -regex '.*RF_0[0|1].+\.vcf' | sort | xargs -P 7 -n 1 ./mongo_vcf.py file_add -s localhost -p 27017 -d vcf; ./mongo_vcf.py index_add -s localhost -p 27017 -d vcf; ./mongo_vcf.py command -s localhost -p 27017 -d vcf compact
#
#
#alias vcfmongo="docker run -i -t -a stdout --rm -v $PWD/data/:/home/user/data:rw --link sauloal_mongoapistats:mongoserver sauloal/mongoapistatsvcfdev"
#vcfmongo db_del -d vcf -y; vcfmongo db_add -d vcf; find data/ -regex '.*\.vcf' | sort | xargs -P 7 -n 1 -I{} bash -c "echo $PWD {}; docker run -i -t -a stdout --rm -v $PWD/data/:/home/user/data:rw --link sauloal_mongoapistats:mongoserver sauloal/mongoapistatsvcfdev file_add -d vcf {}"
#find data/ -regex '.*\.vcf' | sort | parallel --gnu -j7 S=\$\(\( {#} % 7 \)\) \&\&  T=\$\(\( \$S*13 \)\) \&\& echo {} pos {#} mod \$S sleeping \$T \&\& sleep \$T \&\& echo awoke {} pos {#} \&\& docker run -i -t -a stdout --rm -v $PWD/data/:/home/user/data:rw --link sauloal_mongoapistats:mongoserver sauloal/mongoapistatsvcfdev file_add -d vcf {}

######################################
# MAIN FUNCTION CALLING SUBFUNCTIONS
######################################
def main():
    print sys.argv

    modules = [
        [ 'command'     , 'run system command'           ],
        [ 'coords_list' , 'list coordinates information' ],
        [ 'dump_db'     , 'dump database'                ],
        [ 'dump_coords' , 'dump coordinates'             ],
        [ 'dump_files'  , 'dump files'                   ],
        [ 'file_add'    , 'add files'                    ],
        [ 'file_list'   , 'list files'                   ],
        [ 'db_add'      , 'create db'                    ],
        [ 'db_del'      , 'delete db'                    ],
        [ 'db_list'     , 'list dbs'                     ],
        [ 'index_add'   , 'add index'                    ],
        [ 'index_del'   , 'delete index'                 ],
        [ 'index_status', 'list indexes'                 ],
        [ 'query_file'  , 'load JSON query file'         ],
        [ 'query_stdin' , 'load JSON from stdin'         ],
        [ 'print_struct', 'print db structure'           ],
        [ 'status_list' , 'list current status'          ]
    ]

    options = {}
    for mod in modules:
        options[ mod[ 0 ] ] = [ mod[1], globals()[ mod[0] ]() ]


    parser     = argparse.ArgumentParser(description='Add vcf to mongodb')

    subparsers = parser.add_subparsers()

    for mode in options:
        mode_title, mode_func = options[ mode ]
        s_parser = subparsers.add_parser( mode, help=mode_title )
        s_parser.set_defaults( func=mode_func.parse_args )
        mode_func.set_args( s_parser )

    args = parser.parse_args()

    args.func( args )




######################################
# COMMAND LINE PARSERS
######################################
class coords_list( object ):
    def set_args( self, parser ):
        sharedArguments( parser )
        sharedArgumentsQuery( parser )

    def parse_args( self, args ):
        print 'running', self.__class__.__name__
        db = getDb( args.server, args.port, args.db )

        coords = db.coords

        print "COORDS"
        print " Full Name", coords.full_name
        print " Name     ", coords.name
        print " Database ", coords.database
        print " Count    ", coords.count()

class command( object ):
    def set_args( self, parser ):
        self.parser = parser
        sharedArguments( parser )
        sharedArgumentsQuery( parser )
        parser.add_argument('command', type=str, metavar='command', nargs=argparse.REMAINDER, help='raw command: list, log, last, available, uri, feat, build, compact, currop or raw command ' )

    def parse_args( self, args ):
        print 'running', self.__class__.__name__
        print "COMMAND", args.command
        if len( args.command ) == 0:
            print "NO COMMANDS GIVEN"
            self.parser.print_help()
            exit( 1 )

        db = getDb( args.server, args.port, args.db )

        cmd = " ".join( args.command )

        print "CMD '%s'" % ( cmd )

        if cmd == 'list':
            print "LIST COMMANDS"
            pp.pprint( db.command( 'listCommands'          ) )

        elif cmd == 'last':
            print "LAST ERROR"
            pp.pprint( db.command( 'getLastError'          ) )

        elif cmd == 'count':
            print "COUNT ELEMENTS"
            pp.pprint( db.command( 'count'          ) )

        elif cmd == 'log':
            print "LOG"
            pp.pprint( db.command( 'getLog'          ) )

        elif cmd == 'available':
            print "AVAILABLE QUERIES OPTIONS"
            pp.pprint( db.command( 'availableQueryOptions' ) )

        elif cmd == 'uri':
            print "WHAT'S MY URI"
            pp.pprint( db.command( 'whatsmyuri'            ) )

        elif cmd == 'feat':
            print "FEATURES"
            pp.pprint( db.command( 'features'              ) )

        elif cmd == 'host':
            print "HOST INFO"
            pp.pprint( db.command( { 'hostInfo': '1' } ) )

        elif cmd == 'build':
            print "BUILD INFO"
            pp.pprint( db.command("buildinfo") )

        elif cmd == 'compact':
            print "COMPACT DB: files"
            #pp.pprint( db.command( { 'compact': 'files' } ) )

            print "COMPACT DB: coords"
            #pp.pprint( db.command( { 'compact': 'coords' } ) )

        elif cmd == 'currop':
            print "CURRENT OP"
            pp.pprint( db.current_op() )

        else:
            print "RAW",
            if '{' in cmd:
                print "parsing"
                cmd = json.loads( cmd )

            else:
                print "raw"

            pp.pprint( db.command( cmd ) )

class dump_db( object ):
    def set_args( self, parser ):
        sharedArguments( parser )
        sharedArgumentsQuery( parser )

    def parse_args( self, args ):
        print 'running', self.__class__.__name__
        dump_coords().parse_args( args )
        dump_files().parse_args( args )

class dump_coords( object ):
    def set_args( self, parser ):
        sharedArguments( parser )
        sharedArgumentsQuery( parser )

    def parse_args( self, args ):
        print 'running', self.__class__.__name__
        db = getDb( args.server, args.port, args.db )
        mongorepr( db.coords.find(limit=100), desc="DUMP COORDS" )

class dump_files( object ):
    def set_args( self, parser ):
        sharedArguments( parser )
        sharedArgumentsQuery( parser )

    def parse_args( self, args ):
        print 'running', self.__class__.__name__
        db = getDb( args.server, args.port, args.db )
        mongorepr( db.files.find(limit=100), desc="DUMP FILES" )

class file_add( object ):
    def set_args( self, parser ):
        sharedArguments( parser )
        sharedArgumentsQuery( parser )
        parser.add_argument('files', type=str, metavar='*.vcf', nargs=argparse.REMAINDER, help='input files' )
        parser.add_argument('-w', '-si', '--switch-index', dest='switch', action='store_true', help='switch index off and on again' )
        parser.add_argument('-e', '-ei', '--ensure-index', dest='ensure', action='store_true', help='ensure index' )

    def parse_args( self, args ):
        print 'running', self.__class__.__name__
        db = getDb( args.server, args.port, args.db )

        if args.ensure:
            args.switch = False
            index_add().parse_args(args)

        if args.switch:
            index_del().parse_args(args)

        for infile in args.files:
            process_file( db, infile )

        if args.switch:
            index_add().parse_args(args)

class file_list( object ):
    def set_args( self, parser ):
        sharedArguments( parser )
        sharedArgumentsQuery( parser )

    def parse_args( self, args ):
        print 'running', self.__class__.__name__
        db  = getDb( args.server, args.port, args.db )
        res = db.files.find(fields=['file_path']).sort('file_path')
        #mongorepr( res , desc="MAIN DUMP RECORDS FILES AFTER " )
        files  = db.files
        print "FILES"
        print " Full Name", files.full_name
        print " Name     ", files.name
        print " Database ", files.database
        print " Count    ", files.count()

        for filename in res:
            print ' ', filename['file_path']
        return res

class db_add( object ):
    def set_args( self, parser ):
        sharedArguments( parser )
        sharedArgumentsQuery( parser )
        parser.add_argument('-e', '-ei', '--ensure-index', dest='ensure', action='store_true', help='ensure index' )

    def parse_args( self, args ):
        print 'running', self.__class__.__name__
        client = getClient( args.server, args.port )

        db_list().parse_args(args)

        print "db", args.db
        db = client[ args.db ]
        db.create_collection( 'files'   )
        db.create_collection( 'coords'  )
        db.create_collection( 'queries' )
        db.create_collection( 'structs' )
        print db

        addQueries( db )
        addStructs( db )

        if args.ensure:
            index_add().parse_args(args)

        db_list().parse_args(args)


        return db

class db_del( object ):
    def set_args( self, parser ):
        sharedArguments( parser )
        sharedArgumentsQuery( parser )
        parser.add_argument('-y', '--yes', default=False, dest='yes', action='store_true', help='confirmation to delete', required=True )

    def parse_args( self, args ):
        print 'running', self.__class__.__name__
        client = getClient( args.server, args.port )

        print "databases before", db_list().parse_args(args)

        if args.yes:
            print "deleting database", args.db
            client.drop_database( args.db )

        print "databases after", db_list().parse_args(args)

class db_list( object ):
    def set_args( self, parser ):
        sharedArguments( parser )

    def parse_args( self, args ):
        print 'running', self.__class__.__name__
        client = getClient( args.server, args.port )
        names  = client.database_names()
        #print names

        for name in names:
            print "Name", name
            print " Full Name", client[ name ].fullname
            print " Name     ", client[ name ].name
            print " Database ", client[ name ].database

        return names

class index_add( object ):
    def set_args( self, parser ):
        sharedArguments( parser )
        sharedArgumentsQuery( parser )

    def parse_args( self, args ):
        print 'running', self.__class__.__name__
        db = getDb( args.server, args.port, args.db )
        addIndex( db )

class index_del( object ):
    def set_args( self, parser ):
        sharedArguments( parser )
        sharedArgumentsQuery( parser )
        parser.add_argument('-y', '--yes', default=False, action='store_true', help='confirmation to delete' )

    def parse_args( self, args ):
        print 'running', self.__class__.__name__
        db = getDb( args.server, args.port, args.db )
        dropIndex( db )

class index_status( object ):
    def set_args( self, parser ):
        sharedArguments( parser )
        sharedArgumentsQuery( parser )

    def parse_args( self, args ):
        print 'running', self.__class__.__name__
        db = getDb( args.server, args.port, args.db )
        listIndex( db )

class query_file( object ):
    def set_args( self, parser ):
        sharedArguments( parser )
        sharedArgumentsQuery( parser )
        parser.add_argument('-f', '--file', type=str, metavar='qry.json', nargs='?', help='query file', required=True )

    def parse_args( self, args ):
        print 'running', self.__class__.__name__
        db = getDb( args.server, args.port, args.db )

        qry = json.load( open( args.file, 'r' ) )

        print qry

        return query( db, qry )

class query_stdin( object ):
    def set_args( self, parser ):
        sharedArguments( parser )
        sharedArgumentsQuery( parser )

    def parse_args( self, args ):
        print 'running', self.__class__.__name__
        db = getDb( args.server, args.port, args.db )

        qry = ""
        for line in sys.stdin:
            qry += line

        print qry
        jqry = json.loads( qry )
        print jqry

        return query( db, jqry )

class print_struct( object ):
    def set_args( self, parser ):
        sharedArguments( parser )
        sharedArgumentsQuery( parser )

    def parse_args( self, args ):
        print 'running', self.__class__.__name__
        db = getDb( args.server, args.port, args.db )
        printStructs( db )
        printQueries( db )

class status_list( object ):
    def set_args( self, parser ):
        sharedArguments( parser )
        sharedArgumentsQuery( parser )

    def parse_args( self, args ):
        print 'running', self.__class__.__name__

        db_list().parse_args(args)
        file_list().parse_args(args)
        coords_list().parse_args(args)

        db = getDb( args.server, args.port, args.db )

        print "COL STATS: FILES"
        pp.pprint( db.command( { 'collStats': "files" , 'scale' : 1024 } ) )
        print "COL STATS: COORDS"
        pp.pprint( db.command( { 'collStats': "coords", 'scale' : 1024 } ) )

        print "DB STATS"
        pp.pprint( db.command( { 'dbStats': '1' } ) )
        print "SERVER STATUS"
        pp.pprint( db.command( { 'serverStatus': '1' } ) )

        print "DATASIZE: FILES"
        pp.pprint( db.command( { 'dataSize': "vcf.files" } ) )
        print "DATASIZE: COORDS"
        pp.pprint( db.command( { 'dataSize': "vcf.coords" } ) )







######################################
# CORE FUNCTIONS
######################################
def sharedArguments( parser ):
    parser.add_argument( '-s', '--server', dest='server', type=str, nargs='?', default='localhost', metavar='localhost', help='server name: localhost', required=True )
    parser.add_argument( '-p', '--port'  , dest='port'  , type=int, nargs='?', default=27017      , metavar='27017'    , help='server port: 27017'    , required=True )

def sharedArgumentsQuery( parser ):
    parser.add_argument( '-d', '--db'    , dest='db'    , type=str, nargs='?', default='vcf'      , metavar='vcf'      , help='db name: vcf'          , required=True )

def getClient( server, port ):
    client     = MongoClient( server, port )
    return client

def getDb( server, port, db ):
    client = getClient( server, port )
    return client[ db ]

def query( db, qry ):
    res = db.coords.find( qry )
    #.hint( [ ( "chrom"                  , ASCENDING )  , ( "pos"      , ASCENDING ), ( "recs.parent", ASCENDING ) ] )
    print "SEARCHING QRY %s RES"            % str( qry ), res
    print "SEARCHING QRY %s RES COLLECTION" % str( qry ), res.collection
    print "SEARCHING QRY %s RES COUNT     " % str( qry ), res.count()
    #print "SEARCHING QRY %s RES RECORDS   " % str( qry ), res.recs.count()

    print "SEARCHING QRY %s RES EXPLAIN   " % str( qry ), pp.pprint( res.explain() )

    counter = 0
    for r in res:
        counter += 1
        print " RES #%d/%d ret [%d] = records %d" % ( counter, res.count(), res.retrieved, len( r['recs'] ) )
        pp.pprint( r )
        print "="*40

    #mongorepr( res, desc="SEARCHING QRY %s RES DUMP     " % str( qry ) )

    return res

def addIndex( db ):
    startTimeG     = time.time()
    startTime      = startTimeG

    db.files.ensure_index(  [ ( "file_path"              , ASCENDING )  , ( "file_name", ASCENDING ), ( "file_base"  , ASCENDING ) ], name='filename', unique=True )
    db.files.ensure_index(  [ ( "file_name"              , ASCENDING ) ], name='filename' )
    db.files.ensure_index(  [ ( "file_base"              , ASCENDING ) ], name='filebase' )

    currTime       = time.time()
    diffTime       = currTime - startTime
    startTime      = time.time()

    print " ADDING INDEX TO files %6.1f s" % ( diffTime )

    #NUM REGISTERS   14,588,059
    #SINGLE INDEX
    #AVERAGE SPEED   221 SECS | MIN 190 SECS | 76,810 regs/s
    #STD DEV          31 SECS | AVG 221 SECS | 66,131 regs/s
    #                         | MAX 251 SECS | 58,059 regs/s
    #TRI INDEX
    #SPEED           331	SECS	              44,073 regs/s
    #
    #TOTAL TIME	  7,059 SECS = 117.65 MINUTES = 1.96 HOURES
    #              1 hour 57 minutes 39 seconds

    #serverUsed    127.0.0.1:27017
    #db            vcf
    #collections   6
    #objects           14,588,158     14.5M
    #avgObjSize             1,924.6    1.9Kb
    #dataSize      28,075,723,896     26.2Gb
    #storageSize   28,623,576,928     26.7Gb
    #numExtents                42
    #indexes                   37
    #indexSize     17,540,520,592     16.3Gb
    #fileSize      51,448,381,440     47.9Gb
    #nsSizeMB                  16
    #dataFileVersion { "major" : 4 , "minor" : 5}
    #ok                         1



    data = [
        ( ( "CHROM", "POS", "PARENT"      ), 'coord', True  ),  ( ( "FILTER",                     ),    None, False ),
        ( ( "ID",                         ),    None, False ),  ( ( "POS",                        ),    None, False ),
        ( ( "PARENT",                     ),    None, False ),  ( ( "QUAL",                       ),    None, False ),

        ( ( "DATA.is_deletion",           ),    None, False ),  ( ( "DATA.is_indel",              ),    None, False ),
        ( ( "DATA.is_monomorphic",        ),    None, False ),  ( ( "DATA.is_snp",                ),    None, False ),
        ( ( "DATA.is_sv",                 ),    None, False ),  ( ( "DATA.is_sv_precise",         ),    None, False ),
        ( ( "DATA.is_transition",         ),    None, False ),  ( ( "DATA.sv_end",                ),    None, False ),
        ( ( "DATA.call_rate",             ),    None, False ),  ( ( "DATA.end",                   ),    None, False ),
        ( ( "DATA.heterozygosity",        ),    None, False ),  ( ( "DATA.nucl_diversity",        ),    None, False ),
        ( ( "DATA.num_called",            ),    None, False ),  ( ( "DATA.num_het",               ),    None, False ),
        ( ( "DATA.num_hom_alt",           ),    None, False ),  ( ( "DATA.start",                 ),    None, False ),
        ( ( "DATA.var_subtype",           ),    None, False ),  ( ( "DATA.var_type",              ),    None, False ),

        ( ( "INFO.AF1",                   ),    None, False ),  ( ( "INFO.DP",                    ),    None, False ),
        ( ( "INFO.FQ",                    ),    None, False ),  ( ( "INFO.MQ",                    ),    None, False ),

        ( ( "INFO.EFF.1.Effect",          ),    None, False ),  ( ( "INFO.EFF.1.Effect_Impact",   ),    None, False ),
        ( ( "INFO.EFF.1.Functional_Class",),    None, False ),  ( ( "INFO.EFF.1.Gene_Name",       ),    None, False ),
        ( ( "INFO.EFF.1.Transcript_ID",   ),    None, False ),  ( ( "INFO.EFF.1.Exon",            ),    None, False )
    ]

    # 0"Effect",
    # 1"Effect_Impact",
    # 2"Functional_Class",
    # 3"Codon_Change",
    # 4"Amino_Acid_change",
    # 5"Amino_Acid_length",
    # 6"Gene_Name",
    # 7"Transcript_BioType",
    # 8"Gene_Coding",
    # 9"Transcript_ID",
    #10"Exon",
    #11"GenotypeNum",
    #12"ERRORS",
    #13"WARNINGS"



    if nestedDb:
        data2 = []
        for val in data:
            names = []

            for name in val[0]:
                #print "NAME", name
                if name not in ['CHROM', 'POS', 'ID']:
                    name = 'recs.' + name
                names.append( name )

            data2.append( [ names, val[1], val[2] ] )
        data = data2



    for nfo in data:
        cols, name, uniq = nfo
        #print nfo
        cols_d = []

        for col in cols:
            #print " ", col
            cols_d.append( (col, ASCENDING) )

        if name is None:
            name = "+".join( list(cols) )

        name = name.replace('.', '_')
        print " ADDING INDEX TO coords NAMED %s CONTAINING %s UNIQUE: %s" % ( name, str(cols_d), uniq )
        sys.stdout.flush()
        startTime      = time.time()

        db.coords.ensure_index( cols_d, ASCENDING, unique=uniq, name=name )

        currTime       = time.time()
        diffTime       = currTime - startTime
        print ". TOOK %6.1f s" % diffTime



    currTime       = time.time()
    diffTime       = currTime - startTimeG
    print "ADDING INDEX TOOK %6.1f s" % diffTime


    listIndex( db )

def dropIndex( db ):
    db.files.drop_indexes()
    db.coords.drop_indexes()
    listIndex( db )

def listIndex( db ):
    print "coords index information"
    #print " coords size", db.coords.total_index_size()
    pp.pprint( db.coords.index_information() )
    #pp.pprint( db.coords.stats() )

    print "files index information"
    #print " files size", db.files.total_index_size()
    pp.pprint( db.files.index_information()  )
    #pp.pprint( db.files.stats()  )

def addQueries( db ):
    queries = genQueries()

    for query in queries:
        print "inserting qury", query
        db.queries.insert( query )

def getQueries( db ):
    return db.queries

def printQueries( db ):
    pp.pprint( getQueries( db ) )

def addStructs( db ):
    structs = genStructs()
    db.structs.insert( structs )

def getStructs( db ):
    return db.structs

def printStructs( db ):
    pp.pprint( getStructs( db ) )

def diffTime( runid, chrom, startTime, startTimeChrom, startTimeLap, regs, regsChrom, regsLap ):
    currTime       = time.time()

    diffTimeStart  = currTime - startTime
    diffTimeChrom  = currTime - startTimeChrom
    diffTimeLap    = currTime - startTimeLap

    diffRegsStart  = float( regs      )
    diffRegsChrom  = float( regsChrom )
    diffRegsLap    = float( regsLap   )

    speedStart     = diffRegsStart  / diffTimeStart
    speedChrom     = diffRegsChrom  / diffTimeChrom
    speedLap       = diffRegsLap    / diffTimeLap

    print "\
%s :: Time   : Start %9ds      Chrom %s %8ds      Lap %7ds\n\
%s :: Records: Start %9d       Chrom %s %8d       Lap %7d\n\
%s :: Speed  : Start %9.1f rec/s Chrom %s %8.1f rec/s Lap %7.1f rec/s\n\n" % (  runid, diffTimeStart, chrom, diffTimeChrom, diffTimeLap,
                                                                                runid, diffRegsStart, chrom, diffRegsChrom, diffRegsLap,
                                                                                runid, speedStart   , chrom, speedChrom   , speedLap )
    #sys.stdout.flush()

def process_file( db, infile ):
    print "processing", infile

    infile_abs_path = os.path.abspath(  infile )
    infile_basename = os.path.basename( infile )

    try:
        coords_col = db.coords

    except errors.CollectionInvalid:
        pass


    #mongorepr( db.coords.find(), desc="PROCESS DUMP ALL COORDS" )
    #mongorepr( db.files.find() , desc="PROCESS DUMP ALL FILES" )

    #print "PROCESS COLLECTIONS", db.collection_names()
    #print "PROCESS FILES INFO" , db.files.name, db.files.database, db.files.full_name

    vcf_reader = vcf.Reader(open(infile, 'r'))

    alts       = dict( vcf_reader.alts     )
    contigs    = dict( vcf_reader.contigs  )
    filters    = dict( vcf_reader.filters  )
    formats    = dict( vcf_reader.formats  )
    infos      = dict( vcf_reader.infos    )
    meta       = dict( vcf_reader.metadata )

    #print "alts   ", pp.pprint( alts    )
    #print "contigs", pp.pprint( contigs )
    #print "filters", pp.pprint( filters )
    #print "formats", pp.pprint( formats )
    #print "infos  ", pp.pprint( infos   )
    #print "meta   ", pp.pprint( meta    )


    # fix snpeff
    if 'EFF' in infos:
        #print infos

        for k in infos.keys():
            #print k, infos[k]
            #print 'DICT', infos[k].__dict__
            #print 'ASDICT', infos[k]._asdict()
            #, dir(infos[k])
            infos[ k ] = dict( infos[k]._asdict() )

        #print 'INFOS', infos
        #print 'INFOS EFF', infos['EFF']
        #print 'INFOS EFF DESC', infos['EFF']['desc']

        #infos['EFF']['desc'] = [ x.replace('[', '').replace(']', '').replace(')', '').replace('(', '|').strip().split( '|' ) for x in infos['EFF']['desc'] ]
        Spos = infos['EFF']['desc'].find('Format: ')
        if Spos != -1:
            Spos += 8
            infos['EFF']['format'] = infos['EFF']['desc'  ][Spos:]
            infos['EFF']['format'] = infos['EFF']['format'].replace('[', '').replace(']', '').replace(')', '').replace('\'', '').replace('(', '|').split( '|' )
            infos['EFF']['format'] = [ x.strip() for x in infos['EFF']['format'] ]
            #print 'INFOS EFF FORMAT', infos['EFF']['format']


    #pp.pprint( header )

    #mongorepr( db.files.find() , desc="PROCESS DUMP FILES BEFORE" )

    fileId = fixString(infile_abs_path)
    db.files.insert( {
        '_id'        : fileId,
        'file_name'  : infile,
        'file_path'  : infile_abs_path,
        'file_base'  : infile_basename,
        'commit_date': str(datetime.datetime.utcnow().isoformat()),
        'alts'       : alts   ,
        'contigs'    : contigs,
        'filters'    : filters,
        'formats'    : formats,
        'infos'      : infos  ,
        'meta'       : meta,
        'finished'   : False,
        'records'    : 0,
        'chroms'     : {},
        'db_version' : __version__
    })

    #mongorepr( db.files.find() , desc="PROCESS DUMP FILES AFTER" )


    lastChrom      = None
    lastChromUrl   = None
    lastPos        = None
    regs           = 0
    regsChrom      = 0
    regsLap        = 0
    startTime      = time.time()
    startTimeChrom = startTime
    startTimeLap   = startTime
    chroms         = {}

    for record in vcf_reader:
        #print pp.pprint( record )

        regs      += 1
        regsChrom += 1
        regsLap   += 1

        rec = {}
        for k in colnames:
            val = getattr( record, k )
            #print k, val
            rec[ k ] = parseval( val )

        chrom = rec[ 'CHROM' ]
        pos   = rec[ 'POS'   ]
        snpId = rec[ 'ID'    ]


        #fix SNPeff
        if 'INFO' in rec and 'EFF' in rec['INFO']:
            #print "INFO EFF B", rec['INFO']['EFF']
            eff       = rec['INFO']['EFF']
            eff_keys  = infos['EFF']['format']
            vals      = [ (x, x.replace(')', '').replace('(', '|').strip().split( '|' )) for x in eff ]
            info_vals = []

            #print "EFF     ", eff
            #print "EFF KEYS", eff_keys
            #print "VALS    ", vals

            for val in vals:
                val_str  = val[0]
                val_lst  = val[1]
                info_val = { }

                for pkey, key in enumerate( eff_keys ):
                    #print "pkey", pkey, 'key', key, 'val str', val_str, 'val lst', val_lst
                    if pkey >= len(val_lst):
                        info_val[ key ] = ''
                        #print "val str", ''
                    else:
                        info_val[ key ] = val_lst[ pkey ]
                        #print "val str", val_lst[ pkey ]


                info_vals.append( [ val_str, info_val ] )

            rec['INFO']['EFF'] = info_vals

            #print "INFO EFF A", rec['INFO']['EFF']





        #http://pyvcf.readthedocs.org/en/latest/API.html#vcf-model-record
        nfo = {
            'aaf'           : record.aaf,            # A list of allele frequencies of alternate alleles. NOTE: Denominator calc?ed from _called_ genotypes.
            'alleles'       : parseval( record.alleles ),        # list of alleles. [0] = REF, [1:] = ALTS
            'call_rate'     : record.call_rate,      # The fraction of genotypes that were actually called.
            'end'           : record.end,            # 1-based end coordinate
            #'genotype': genotype("name")           # Lookup a _Call for the sample given in name
            #get_hets: get_hets()                   # The list of het genotypes
            #get_hom_alts: get_hom_alts()           # The list of hom alt genotypes
            #get_hom_refs: get_hom_refs()           # The list of hom ref genotypes
            #get_unknowns: get_unknowns()           # The list of unknown genotypes
            'heterozygosity': record.heterozygosity, # Heterozygosity of a site. Heterozygosity gives the probability that two randomly chosen chromosomes from the population have different alleles, giving a measure of the degree of polymorphism in a population. If there are i alleles with frequency p_i, H=1-sum_i(p_i^2)
            'is_deletion'   : record.is_deletion,    # Return whether or not the INDEL is a deletion
            'is_indel'      : record.is_indel,       # Return whether or not the variant is an INDEL
            'is_monomorphic': record.is_monomorphic, # Return True for reference calls
            'is_snp'        : record.is_snp,         # Return whether or not the variant is a SNP
            'is_sv'         : record.is_sv,          # Return whether or not the variant is a structural variant
            'is_sv_precise' : record.is_sv_precise,  # Return whether the SV cordinates are mapped to 1 b.p. resolution.
            'is_transition' : record.is_transition,  # Return whether or not the SNP is a transition
            'nucl_diversity': record.nucl_diversity, # pi_hat (estimation of nucleotide diversity) for the site. This metric can be summed across multiple sites to compute regional nucleotide diversity estimates. For example, pi_hat for all variants in a given gene.
            'num_called'    : record.num_called,     # The number of called samples
            'num_het'       : record.num_het,        # The number of heterozygous genotypes
            'num_hom_alt'   : record.num_hom_alt,    # The number of homozygous for alt allele genotypes
            'num_hom_ref'   : record.num_hom_ref,    # The number of homozygous for ref allele genotypes
            'num_unknown'   : record.num_unknown,    # The number of unknown genotypes
            #'samples'       : record.samples,        # list of _Calls for each sample ordered as in source VCF
            'start'         : record.start,          # 0-based start coordinate
            'sv_end'        : record.sv_end,         # Return the end position for the SV
            'var_subtype'   : record.var_subtype,    # Return the subtype of variant. - For SNPs and INDELs, yeild one of: [ts, tv, ins, del] - For SVs yield either ?complex? or the SV type defined in the ALT fields (removing the brackets). E.g.:
            'var_type'      : record.var_type        # Return the type of variant [snp, indel, unknown] TO DO: support SVs
        }

        get_hets     = record.get_hets()
        get_hom_alts = record.get_hom_alts()
        get_hom_refs = record.get_hom_refs()
        get_unknowns = record.get_unknowns()
        samples      = record.samples

        nfo[ 'get_hets'     ] = sample2dict( get_hets     )
        nfo[ 'get_hom_alts' ] = sample2dict( get_hom_alts )
        nfo[ 'get_hom_refs' ] = sample2dict( get_hom_refs )
        nfo[ 'get_unknowns' ] = sample2dict( get_unknowns )
        nfo[ 'samples'      ] = sample2dict( samples      )



        rec[ 'DATA'         ] = nfo
        rec[ '_id'          ] = "%09".join( [ fixString(infile_abs_path), fixString(chrom), "%012d"%pos ] )

        rec[ 'PARENT'       ] = fixString(infile_abs_path)



        if lastChrom != chrom:
            if lastChrom is not None:
                print "\n", infile, chrom
                diffTime( infile, chrom, startTime, startTimeChrom, startTimeLap, regs, regsChrom, regsLap )
                chroms[ fixString(lastChrom) ] = {
                    "regs_chrom": regsChrom,
                    "time"      : time.time() - startTimeChrom
                }


            startTimeLap   = time.time()
            startTimeChrom = startTimeLap

            lastChrom      = chrom
            regsChrom      = 0
            regsLap        = 0

            #if regs != 1:
            #    break


        if nestedDb:
            coords_col.update( {
                                    '_id'  : fixString(chrom) + "%09" + "%012d"%pos,
                                    'CHROM': chrom,
                                    'POS'  : pos,
                                    'ID'   : snpId
                                },
                                {
                                    '$push': { 'recs': rec }
                                }, True )

        else:
            coords_col.insert( rec )



        if regs % print_every == 0:
            #sys.stdout.write( str( pos ) + " " )
            diffTime( infile, chrom, startTime, startTimeChrom, startTimeLap, regs, regsChrom, regsLap )
            regsLap      = 0
            startTimeLap = time.time()


        if regs == read_only:
            print "DUMPING"

            diffTime( infile, chrom, startTime, startTimeChrom, startTimeLap, regs, regsChrom, regsLap )

            if dump_file:
                mongorepr( db.coords.find(), desc="PROCESS DUMP ALL COORDS" )
                mongorepr( db.files.find() , desc="PROCESS DUMP ALL FILES" )
            break


    chroms[ fixString(lastChrom) ] = {
        "regs_chrom": regsChrom,
        "time"      : time.time() - startTimeChrom
    }

    db.files.update(
                        { '_id': fileId },
                        {
                            'finished'   : True,
                            'records'    : regs,
                            'chroms'     : chroms
                        }
                    )

def genQueries():
    queries = [
        {
            'name'         : 'get_pos',
            'input_fields' : { 'CHROM': 'str', 'POS': 'int' },
            #'output_format': genStructs(),
            'query format' : "{ 'CHROM': '%CHROM%', 'POS': '%POS%' }"
        },
        {
            'name'         : 'get_range',
            'input_fields' : { 'CHROM': 'str', 'POS_START': 'int', 'POS_END': 'int' },
            #'output_format': genStructs(),
            'query format' : "{ 'CHROM': '%CHROM%', 'POS': { '$gt': '%POS_START%', '$lt': '%POS_END%' } }"
        },
    ]

    return queries

def genStructs():
    structs = {
        'files': {
                'id': '%2Fhome%2Fassembly%2Ftomato150%2Fprograms%2Fmongo%2Fdata%2FRF_001_SZAXPI008746-45%2Evcf%2Egz%2Esnpeff%2Evcf',
                'alts': {},
                'commit_date': '2014-05-14T21:50:00.021071',
                'contigs': {},
                'file_base': 'RF_001_SZAXPI008746-45.vcf.gz.snpeff.vcf',
                'file_name': '../data/RF_001_SZAXPI008746-45.vcf.gz.snpeff.vcf',
                'file_path': '/home/assembly/tomato150/programs/mongo/data/RF_001_SZAXPI008746-45.vcf.gz.snpeff.vcf',
                'filters': {},
                'formats': {
                                'DP': ['DP', 1, 'Integer', '# high-quality bases'],
                                'GL': ['GL',
                                        3,
                                        'Float',
                                        'Likelihoods for RR,RA,AA genotypes (R=ref,A=alt)'],
                                'GQ': ['GQ', 1, 'Integer', 'Genotype Quality'],
                                'GT': ['GT', 1, 'String', 'Genotype'],
                                'PL': ['PL',
                                        -1,
                                        'Integer',
                                        'List of Phred-scaled genotype likelihoods, number of values is (#ALT+1)*(#ALT+2)/2'],
                                'SP': ['SP',
                                        1,
                                        'Integer',
                                        'Phred-scaled strand bias P-value']
                            },
                'infos': {
                            'AF1': {'desc': 'Max-likelihood estimate of the site allele frequency of the first ALT allele',
                                    'id': 'AF1',
                                    'num': 1,
                                    'type': 'Float'},
                            'CI95': {'desc': 'Equal-tail Bayesian credible interval of the site allele frequency at the 95% level',
                                     'id': 'CI95',
                                     'num': 2,
                                     'type': 'Float'},
                            'DP': {'desc': 'Raw read depth',
                                   'id': 'DP',
                                   'num': 1,
                                   'type': 'Integer'},
                            'DP4': {'desc': '# high-quality ref-forward bases, ref-reverse, alt-forward and alt-reverse bases',
                                    'id': 'DP4',
                                    'num': 4,
                                    'type': 'Integer'},
                            'EFF': {'desc': u"Predicted effects for this variant.Format: 'Effect ( Effect_Impact | Functional_Class | Codon_Change | Amino_Acid_change| Amino_Acid_length | Gene_Name | Transcript_BioType | Gene_Coding | Transcript_ID | Exon  | GenotypeNum [ | ERRORS | WARNINGS ] )' ",
                                    'format': ['Effect',
                                                'Effect_Impact',
                                                'Functional_Class',
                                                'Codon_Change',
                                                'Amino_Acid_change',
                                                'Amino_Acid_length',
                                                'Gene_Name',
                                                'Transcript_BioType',
                                                'Gene_Coding',
                                                'Transcript_ID',
                                                'Exon',
                                                'GenotypeNum',
                                                'ERRORS',
                                                'WARNINGS'],
                                    'id': 'EFF',
                                    'num': None,
                                    'type': 'String'},
                            'FQ': {'desc': 'Phred probability of all samples being the same',
                                   'id': 'FQ',
                                   'num': 1,
                                   'type': 'Float'},
                            'INDEL': {'desc': 'Indicates that the variant is an INDEL.',
                                      'id': 'INDEL',
                                      'num': 0,
                                      'type': 'Flag'},
                            'MQ': {'desc': 'Root-mean-square mapping quality of covering reads',
                                   'id': 'MQ',
                                   'num': 1,
                                   'type': 'Integer'},
                            'PC2': {'desc': 'Phred probability of the nonRef allele frequency in group1 samples being larger (,smaller) than in group2.',
                                    'id': 'PC2',
                                    'num': 2,
                                    'type': 'Integer'},
                            'PCHI2': {'desc': 'Posterior weighted chi^2 P-value for testing the association between group1 and group2 samples.',
                                      'id': 'PCHI2',
                                      'num': 1,
                                      'type': 'Float'},
                            'PV4': {'desc': 'P-values for strand bias, baseQ bias, mapQ bias and tail distance bias',
                                    'id': 'PV4',
                                    'num': 4,
                                    'type': 'Float'},
                            'QCHI2': {'desc': 'Phred scaled PCHI2.',
                                      'id': 'QCHI2',
                                      'num': 1,
                                      'type': 'Integer'},
                            'RP': {'desc': '# permutations yielding a smaller PCHI2.',
                                   'id': 'RP',
                                   'num': 1,
                                   'type': 'Integer'}},
                'meta': {
                            'SnpEffCmd': ['"SnpEff  -no-upstream -no-downstream -csvStats Slyc2.40 "'],
                            'SnpEffVersion': ['"3.3h (build 2013-08-20), by Pablo Cingolani"'],
                            'fileformat': 'VCFv4.1',
                            'samtoolsVersion': ['0.1.14 (r933:170)']
                        },
                'records'   : 10,
                'chroms'    : {
                    'SL2%4640ch00': {
                        "regs_chrom": 10,
                        "time"      : 10
                    }
                },
                'db_version': __version__
        },
        'coords': {
            'ALT': ['C'],
            'CHROM': 'SL2.40ch00',
            'DATA': {
                        'aaf': [1.0],
                        'alleles': ['A', 'C'],
                        'call_rate': 1.0,
                        'end': 4314,
                        'get_hets': [],
                        'get_hom_alts': [
                                            {
                                                'called': True,
                                                'data': {'DP': 26,
                                                          'GQ': 99,
                                                          'GT': '1/1',
                                                          'PL': [255, 78, 0]},
                                                'gt_alleles': ['1', '1'],
                                                'gt_bases': 'C/C',
                                                'gt_type': 2,
                                                'is_het': False,
                                                'is_variant': True,
                                                'phased': False,
                                                'sample': '/panfs/ANIMAL/group001/minjiumeng/tomato_reseq/SZAXPI008746-45'
                                            }
                                        ],
                        'get_hom_refs': [],
                        'get_unknowns': [],
                        'heterozygosity': 0.0,
                        'is_deletion': False,
                        'is_indel': False,
                        'is_monomorphic': False,
                        'is_snp': True,
                        'is_sv': False,
                        'is_sv_precise': False,
                        'is_transition': False,
                        'nucl_diversity': 0.0,
                        'num_called': 1,
                        'num_het': 0,
                        'num_hom_alt': 1,
                        'num_hom_ref': 0,
                        'num_unknown': 0,
                        'samples': [
                                        {
                                            'called': True,
                                            'data': {'DP': 26,
                                                      'GQ': 99,
                                                      'GT': '1/1',
                                                      'PL': [255, 78, 0]},
                                            'gt_alleles': ['1', '1'],
                                            'gt_bases': 'C/C',
                                            'gt_type': 2,
                                            'is_het': False,
                                            'is_variant': True,
                                            'phased': False,
                                            'sample': '/panfs/ANIMAL/group001/minjiumeng/tomato_reseq/SZAXPI008746-45'
                                        }
                                    ],
                        'start': 4313,
                        'sv_end': None,
                        'var_subtype': 'tv',
                        'var_type': 'snp'
                    },
            'FILTER': None,
            'FORMAT': 'GT:PL:DP:GQ',
            'ID': None,
            'INFO': {
                        'AF1': 1.0,
                        'CI95': [1.0, 1.0],
                        'DP': 31,
                        'DP4': [0, 0, 15, 11],
                        'EFF': [['INTERGENIC(MODIFIER||||||||||1)',
                                  {'Amino_Acid_change': '',
                                   'Amino_Acid_length': '',
                                   'Codon_Change': '',
                                   'ERRORS': '',
                                   'Effect': 'INTERGENIC',
                                   'Effect_Impact': 'MODIFIER',
                                   'Exon': '',
                                   'Functional_Class': '',
                                   'Gene_Coding': '',
                                   'Gene_Name': '',
                                   'GenotypeNum': '1',
                                   'Transcript_BioType': '',
                                   'Transcript_ID': '',
                                   'WARNINGS': ''}]],
                        'FQ': -105.0,
                        'MQ': 60
                    },
            'PARENT': '%2Fhome%2Fassembly%2Ftomato150%2Fprograms%2Fmongo%2Fdata%2FRF_001_SZAXPI008746-45%2Evcf%2Egz%2Esnpeff%2Evcf',
            'POS': 4314,
            'QUAL': 222.0,
            'REF': 'A',
            'id': '%2Fhome%2Fassembly%2Ftomato150%2Fprograms%2Fmongo%2Fdata%2FRF_001_SZAXPI008746-45%2Evcf%2Egz%2Esnpeff%2Evcf%09SL2%2E40ch00%09000000004314'
        }
    }

    return structs










######################################
# HELP FUNCTIONS
######################################
# The web framework gets post_id from the URL and passes it as a string
def get( src, post_id ):
    	# Convert from string to ObjectId:
    	document = src.find_one( {'_id': ObjectId(post_id) } )
	return document


def parseval( val ):
    if repr(type(val)) == "<type 'list'>":
        res = []
        for e in val:
            #print e, type(e)
            res.append( str(e) )
        return res
    else:
        return val


def mongorepr( res, desc="" ):
    if desc != "":
        print desc,
    pp.pprint( json.loads( json_util.dumps( res ) ) )


def sample2dict( samples ):
    #print samples

    res = []
    for sample in samples:
        re = {
            'called'    : sample.called,     # True if the GT is not ./.
            'gt_alleles': sample.gt_alleles, # The numbers of the alleles called at a given sample
            'gt_bases'  : sample.gt_bases,   # The actual genotype alleles. E.g. if VCF genotype is 0/1, return A/G
            'gt_type'   : sample.gt_type,    # The type of genotype. hom_ref = 0 het = 1 hom_alt = 2 (we don;t track _which+ ALT) uncalled = None
            'is_het'    : sample.is_het,     # Return True for heterozygous calls
            'is_variant': sample.is_variant, # Return True if not a reference call
            'phased'    : sample.phased,     # A boolean indicating whether or not the genotype is phased for this sample
            'sample'    : sample.sample,     # The sample name
            #'site'      : sample.site,       # The _Record for this _Call
        }

        re['data'] = calldata2dict( sample.data ) # Dictionary of data from the VCF file

        res.append( re )

    return res


def calldata2dict( data ):
    res = {}

    for field in data._fields:
        res[ field ] = getattr(data, field)

    #pp.pprint( data )
    #pp.pprint( res  )

    return res


def fixString( text ):
    return urllib.quote_plus( text ).replace('.', '%2E')










#    lastChromUrl = urllib.quote_plus(chrom).replace('.', '%2E')
#    #print chrom, lastChromUrl
#
#    if coords_col.find( { '_id': lastChromUrl } ).count() == 0:
#        print " chrom", chrom, "does not exists. adding"
#        mongorepr( coords_col.find( { '_id': lastChromUrl } ), desc="CHROM NOT EXISTS BEFORE" )
#
#        coords_col.insert( { '_id': lastChromUrl } )
#
#        mongorepr( coords_col.find(), desc="CHROM NOT EXISTS AFTER coll all" )
#
#    else:
#        print " chrom ", chrom, "already exists. skipping"
#        mongorepr( coords_col.find( { '_id': lastChromUrl } ), desc="CHROM EXISTS" )
#
#    lastPos     = None
#
#    #mongorepr( coords_col.find(), desc="DUMP ALL" )
#



#if lastPos != pos:
#    lastPos   = pos
#
#    if coords_col.find( { "_id": lastChrom, 'poses._id': str(pos) } ).count() == 0:
#        print "pos", pos, "does not exists"
#
#        mongorepr( coords_col.find( { "_id": lastChrom, 'poses._id': str(pos) } ), desc="POST NOS EXISTS BEFORE" )
#
#        coords_col.find_and_modify( { '_id':  lastChrom }, { '$push': { '_oid': str(pos+0) } } )
#        coords_col.find_and_modify( { '_id':  lastChrom }, { '$push': { '_oid': str(pos+1) } } )
#
#        mongorepr( coords_col.find( { "_id": lastChrom, 'poses._id': str(pos) } ), desc="POS NOT EXISTS AFTER" )
#
#    else:
#        print "pos", pos, "exists"
#        mongorepr( coords_col.find( { "_id": lastChrom, 'poses._id': str(pos) } ), desc="POS EXISTS" )

#coords_col.find_and_modify( query={ '_id': lastChrom }, update={ '$push': { 'poses': rec } }, upsert=True )
#coords_col.find_and_modify( query={ '_id': lastChromUrl }, update={ '$set': { lastChromUrl+"."+str(pos): rec } }, upsert=True )







#{ compact: <collection name> }


#pp.pprint( db.command( 'top'                   ) )

#listCommands
#availableQueryOptions
#whatsmyuri
#features
#{ collStats: "collection" , scale : 1024 }
#{ dbStats: 1, scale: 1024 }
#{ "hostInfo" : 1 }
#{ serverStatus: 1 }
#{ dataSize: "database.collection" }

#top
#{ repairDatabase: 1 }
#
#db.command("buildinfo")
#db.command("collstats", collection_name)
#db.command("filemd5", object_id, root=file_root)




#post_id = posts.insert(post)
#
#print "id", post_id
#
#print "find one", posts.find_one()
#
#print "find one mike", posts.find_one({"author": "Mike"})
#
#print "find one eliot", posts.find_one({"author": "Eliot"})
#
#print "find one id", posts.find_one({"_id": post_id})
#
#print "find one id get", get( posts, str(post_id) )
#
#new_posts = [
#        {
#            "author": "Mike",
#                    "text": "Another post!",
#                    "tags": ["bulk", "insert"],
#                    "date": datetime.datetime(2009, 11, 12, 11, 14)
#        },
#                {
#            "author": "Eliot",
#                    "title": "MongoDB is fun",
#                    "text": "and pretty easy too!",
#                    "date": datetime.datetime(2009, 11, 10, 10, 45)
#        }
#    ]
#
#print "new posts", new_posts
#
#posts_ids = posts.insert(new_posts)
#
#print "posts ids", posts_ids
#
#for post in posts.find():
#    print "find", post
#
#for post in posts.find({"author": "Mike"}):
#    print "find mikes", post
#
#print "count", posts.count()
#
#print "count mikes", posts.find({"author": "Mike"}).count()
#
#d = datetime.datetime(2009, 11, 12, 12)
#for post in posts.find({"date": {"$lt": d}}).sort("author"):
#    print "old post", post
#
#print "cursor", posts.find({"date": {"$lt": d}}).sort("author").explain()["cursor"]
#
#print "nscanned", posts.find({"date": {"$lt": d}}).sort("author").explain()["nscanned"]
#
#print "indexing", posts.create_index([("date", DESCENDING), ("author", ASCENDING)])
#
#print "cursor index", posts.find({"date": {"$lt": d}}).sort("author").explain()["cursor"]
#
#print "nscanned index", posts.find({"date": {"$lt": d}}).sort("author").explain()["nscanned"]
#
##http://docs.mongodb.org/manual/reference/sql-comparison/
##db.users.update(
##   { age: { $gt: 25 } },
##   { $set: { status: "C" } },
##   { multi: true }
##)
#
#posts.remove( {} )
#
#print "count", posts.count()
#
#db.drop_collection( "posts" )
#
#print "collections", db.collection_names()
#
#print "databases", client.database_names()
#
#client.drop_database( "saulo_test_database" )
#
#print "databases", client.database_names()







#def search( db ):
#    print "SEARCHING"
#    #http://docs.mongodb.org/manual/reference/crud/
#
#    searches = [
#        { 'CHROM': 'SL2.40ch00', 'POS': { '$gt': 1000, '$lt': 5000 } },
#        { 'CHROM': 'SL2.40ch00', 'POS': 4314 },
#    ]
#
#    for qry in searches:
#        res = db.coords.find( qry )
#        print "SEARCHING QRY %s RES"            % str( qry ), res
#        print "SEARCHING QRY %s RES COLLECTION" % str( qry ), res.collection
#        print "SEARCHING QRY %s RES COUNT     " % str( qry ), res.count()
#        #print "SEARCHING res EXPLAIN   ", res.explain()
#
#        #for r in res:
#        #    print "SEARCHING QRY %s RES #%d/%d = " % ( str( qry ), res.count(), res.count() ), r
#
#        mongorepr( res, desc="SEARCHING QRY %s RES DUMP     " % str( qry ) )
#
#    #mongorepr( db.files.find() , desc="MAIN DUMP RECORDS FILES AFTER " )
#







#def some():
#    print "databases after"  , client.database_names()
#    print "collections before", db.collection_names()
#
#    try:
#        db.create_collection( 'files'  )
#
#    except errors.CollectionInvalid:
#        pass
#
#    try:
#        db.create_collection( 'coords' )
#
#    except errors.CollectionInvalid:
#        pass
#
#    print "collections after", db.collection_names()
#
#    print "collections after index" , db.collection_names()
#
#    #mongorepr( db.files.find() , desc="MAIN DUMP RECORDS FILES BEFORE" )
#    #mongorepr( db.coords.find(), desc="MAIN DUMP RECORDS COORDS BEFORE" )
#
#    for infile in sys.argv[1:]:
#        process_file( db, infile )
#
#    if dump_final:
#        mongorepr( db.coords.find(), desc="MAIN DUMP RECORDS COORDS AFTER " )
#        mongorepr( db.files.find() , desc="MAIN DUMP RECORDS FILES AFTER " )
#
#    search( db )
#
#    client.close()

if __name__ == "__main__":
    main()
