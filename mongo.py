#!/usr/bin/python
#http://api.mongodb.org/python/current/tutorial.html

#import pymongo


from pymongo       import MongoClient
from pymongo       import ASCENDING, DESCENDING
from bson.objectid import ObjectId

import datetime


# The web framework gets post_id from the URL and passes it as a string
def get( src, post_id ):
    	# Convert from string to ObjectId:
    	document = src.find_one( {'_id': ObjectId(post_id) } )
	return document


#client = MongoClient()
client     = MongoClient('localhost', 27017)

print "databases", client.database_names()

db         = client.saulo_test_database

print "collections", db.collection_names()

db.create_collection( "posts" )

posts   = db.posts

print "collections", db.collection_names()

post = 	{
		"author": "Mike",
         	"text"  : "My first blog post!",
         	"tags"  : ["mongodb", "python", "pymongo"],
         	"date"  : datetime.datetime.utcnow()
	}

post_id = posts.insert(post)

print "id", post_id

print "find one", posts.find_one()

print "find one mike", posts.find_one({"author": "Mike"})

print "find one eliot", posts.find_one({"author": "Eliot"})

print "find one id", posts.find_one({"_id": post_id})

print "find one id get", get( posts, str(post_id) )

new_posts = [
		{
			"author": "Mike",
               		"text": "Another post!",
               		"tags": ["bulk", "insert"],
               		"date": datetime.datetime(2009, 11, 12, 11, 14)
		},
              	{
			"author": "Eliot",
               		"title": "MongoDB is fun",
               		"text": "and pretty easy too!",
               		"date": datetime.datetime(2009, 11, 10, 10, 45)
		}
	]

print "new posts", new_posts

posts_ids = posts.insert(new_posts)

print "posts ids", posts_ids

for post in posts.find():
	print "find", post

for post in posts.find({"author": "Mike"}):
	print "find mikes", post

print "count", posts.count()

print "count mikes", posts.find({"author": "Mike"}).count()

d = datetime.datetime(2009, 11, 12, 12)
for post in posts.find({"date": {"$lt": d}}).sort("author"):
	print "old post", post

print "cursor", posts.find({"date": {"$lt": d}}).sort("author").explain()["cursor"]

print "nscanned", posts.find({"date": {"$lt": d}}).sort("author").explain()["nscanned"]

print "indexing", posts.create_index([("date", DESCENDING), ("author", ASCENDING)])

print "cursor index", posts.find({"date": {"$lt": d}}).sort("author").explain()["cursor"]

print "nscanned index", posts.find({"date": {"$lt": d}}).sort("author").explain()["nscanned"]

#http://docs.mongodb.org/manual/reference/sql-comparison/
#db.users.update(
#   { age: { $gt: 25 } },
#   { $set: { status: "C" } },
#   { multi: true }
#)

posts.remove( {} )

print "count", posts.count()

db.drop_collection( "posts" )

print "collections", db.collection_names()

print "databases", client.database_names()

client.drop_database( "saulo_test_database" )

print "databases", client.database_names()



#http://docs.mongodb.org/manual/tutorial/model-tree-structures-with-materialized-paths/
#db.addresses.ensureIndex( { "xmpp_id": 1 }, { sparse: true } )

#db.categories.insert( { _id: "MongoDB", parent: "Databases" } )
#db.categories.insert( { _id: "dbm", parent: "Databases" } )
#
#The query to retrieve the parent of a node is fast and straightforward:
#db.categories.findOne( { _id: "MongoDB" } ).parent
#You can create an index on the field parent to enable fast search by the parent node:
#db.categories.ensureIndex( { parent: 1 } )
#You can query by the parent field to find its immediate children nodes:
#db.categories.find( { parent: "Databases" } )



#db.categories.insert( { _id: "MongoDB", children: [] } )
#db.categories.insert( { _id: "dbm", children: [] } )
#db.categories.insert( { _id: "Databases", children: [ "MongoDB", "dbm" ] } )
#db.categories.insert( { _id: "Languages", children: [] } )
#The query to retrieve the immediate children of a node is fast and straightforward:
#db.categories.findOne( { _id: "Databases" } ).children
#You can create an index on the field children to enable fast search by the child nodes:
#db.categories.ensureIndex( { children: 1 } )
#You can query for a node in the children field to find its parent node as well as its siblings:
#db.categories.find( { children: "MongoDB" } )



#db.categories.insert( { _id: "MongoDB", ancestors: [ "Books", "Programming", "Databases" ], parent: "Databases" } )
#db.categories.insert( { _id: "dbm", ancestors: [ "Books", "Programming", "Databases" ], parent: "Databases" } )
#db.categories.insert( { _id: "Databases", ancestors: [ "Books", "Programming" ], parent: "Programming" } )
#db.categories.insert( { _id: "Languages", ancestors: [ "Books", "Programming" ], parent: "Programming" } )
#db.categories.insert( { _id: "Programming", ancestors: [ "Books" ], parent: "Books" } )
#db.categories.insert( { _id: "Books", ancestors: [ ], parent: null } )
#The query to retrieve the ancestors or path of a node is fast and straightforward:
#db.categories.findOne( { _id: "MongoDB" } ).ancestors
#You can create an index on the field ancestors to enable fast search by the ancestors nodes:
#db.categories.ensureIndex( { ancestors: 1 } )
#You can query by the field ancestors to find all its descendants:
#db.categories.find( { ancestors: "Programming" } )



#db.categories.insert( { _id: "Books", path: null } )
#db.categories.insert( { _id: "Programming", path: ",Books," } )
#db.categories.insert( { _id: "Databases", path: ",Books,Programming," } )
#db.categories.insert( { _id: "Languages", path: ",Books,Programming," } )
#db.categories.insert( { _id: "MongoDB", path: ",Books,Programming,Databases," } )
#db.categories.insert( { _id: "dbm", path: ",Books,Programming,Databases," } )
#You can query to retrieve the whole tree, sorting by the field path:
#db.categories.find().sort( { path: 1 } )
#You can use regular expressions on the path field to find the descendants of Programming:
#db.categories.find( { path: /,Programming,/ } )
#You can also retrieve the descendants of Books where the Books is also at the topmost level of the hierarchy:
#db.categories.find( { path: /^,Books,/ } )
#To create an index on the field path use the following invocation:
#db.categories.ensureIndex( { path: 1 } )
#This index may improve performance depending on the query:
#For queries of the Books sub-tree (e.g. /^,Books,/) an index on the path field improves the query performance significantly.
#For queries of the Programming sub-tree (e.g. /,Programming,/), or similar queries of sub-tress, where the node might be in the middle of the indexed string, the query must inspect the entire index.
#For these queries an index may provide some performance improvement if the index is significantly smaller than the entire collection.
