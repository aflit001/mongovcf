#!/usr/bin/python

from datetime import datetime
from couchdb.mapping import Mapping, Document, TextField, IntegerField, LongField, BooleanField, DictField, ListField, DateTimeField

import couchdb



class Person( Document ):
	name   = TextField()
	age    = IntegerField()
	author = DictField( Mapping.build(
		name  = TextField(),
		email = TextField()
		)
	)
	extra  = DictField()
	comments = ListField( DictField(Mapping.build(
		author  =  TextField(),
		comment =  TextField(),
		time    =  DateTimeField(),
	)))
	added  = DateTimeField( default=datetime.now )

def main():
	dburl = 'http://admin:priwur@127.0.0.1:6984/'
	
	simple(  dburl )
	classed( dburl )


def classed( dburl ):
	couch = couchdb.Server( dburl )
	db    = couch.create( 'test-classed' )

	person = Person(name='John Doe', age=42)
	print person.store( db )
	print person.age

	person = Person.load( db, person.id )
	print person.rev, person.name, person.age, person.added
	person.name = 'John R. Doe'
	person.store(db)

	person = Person.load( db, person.id )
	print person.rev, person.name, person.age, person.added


	couch.delete( 'test-classed' )


def simple( dburl ):
	couch = couchdb.Server( dburl )

	print couch.config()
	print couch.stats()
	print couch.tasks()
	print couch.version()

	db = couch.create( 'test' )
	#db = couch[ 'test' ]

	print db.name
	print db.info
	#print db.

	doc = { 'foo': 'bar' }
	id = db.save( doc )[0]

	print id

	print db[ id ]

	for id in db:
		print "getting", id, db[id]

	doc = db[ id ]
	doc[ 'foo' ] = 'barbar'

	for id in db:
		print "modding", id, db[id]

	db[ 'myid' ] = { 'foofoo': 'barbarbar' }

	for id in db:
		print "myid", id, db[id]

	db.delete( doc )

	for id in db:
		print 'del doc', id, db[id]

	couch.delete( 'test' )




	if 'python-tests' in couch:
		del couch['python-tests']

	db = couch.create('python-tests')
	db['johndoe' ] = { 'type': 'Person', 'name': 'John Doe'   }
	db['maryjane'] = { 'type': 'Person', 'name': 'Mary Jane'  }
	db['gotham'  ] = { 'type': 'City'  , 'name': 'Gotham City'}
	map_fun = '''function(doc) {
		if (doc.type == 'Person')
		emit(doc.name, null);
	}'''

	for row in db.query(map_fun):
		print row.key

	#couch.delete('python-tests')
	del couch['python-tests']


if __name__ == "__main__":
	main()
