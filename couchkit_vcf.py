#!/usr/bin/python
#http://api.mongodb.org/python/current/tutorial.html
import sys
import os
import pprint
from itertools import izip
import json
import urllib

import vcf

import couchdbkit

import datetime

pp = pprint.PrettyPrinter(indent=1)

colnames = ['CHROM', 'POS', 'ID', 'REF', 'ALT', 'QUAL', 'FILTER', 'INFO', 'FORMAT' ]


def main():
    dburl      = 'http://admin:priwur@127.0.0.1:6984/'
    #http://docs.cloudant.com/using-cloudant-with/python.html

    server = couchdbkit.Server( dburl )
    db     = server.get_or_create_db( 'vcf' )

    print 'info', db.info, str(db.info)
    #db.doc_exists(docid)
    #db.open_doc(doc_id)
    #db.get(doc_id)
    #db.list(list_name, view_name)
    #db.show(show_name, doc_id)
    print 'all_docs', db.all_docs(), str(db.all_docs())
    for doc in db.all_docs():
        print 'all_docs', doc, str(doc)

    #db.save_doc( doc )
    #db.save_docs( [doc, doc])
    #db.delete_doc
    #db.bulk_delete
    #db.raw_view(view_path)
    #db.search
    print 'documents', db.documents(), str(db.documents())

    for doc in db.documents():
        print 'documents', doc, str(doc)


    db.files  = {}
    db.coords = {}

    db.ensure_full_commit()
    return

    db.save_doc({
            'author': 'Mike Broberg',
            'content': "In my younger and more vulnerable years, my father told me, 'Son, turn that racket down.'"
    })

    db.save_doc(
            {
                    'chrom00': {
                                    1024: {
                                            'ref': 'A',
                                            'tgt': 'T',
                                            'nfo': {
                                                    'q': 100,
                                                    'v': 200,
                                                    'x': [300,400,500]
                                            }
                                    }
                            }
            }
    )

    db.save_doc(
            {
                    'chrom01': {
                                    1025: {
                                            'ref': 'C',
                                            'tgt': 'G',
                                            'nfo': {
                                                    'q': 600,
                                                    'v': 700,
                                                    'x': [800,900,1000]
                                            }
                                    }
                            }
            }
    )

    # save lots of docs :D
    #docs = [{...}, {...}, {...}]
    #db.bulk_save(docs)


    # associate posts with a given database object
    Post.set_db(db)
    new_post = Post(
            author="Mike Broberg",
            content="In his spare time, Mike Broberg enjoys barbecuing meats of various origins."
    )

    # save the post to its associated database
    new_post.save()


    # print all docs in the database
    for doc in db.view('_all_docs'):
            print 'ALL', doc

    # or, do the same on a pre-defined index
    # where 'DDOC/INDEX' maps to '_design/DDOC/_view/INDEX'
    # in the HTTP API
#       for doc in db.view('DDOC/INDEX'):
#               print doc




#
#    if False:
#        print "databases", client.database_names()
#        client.drop_database( "vcf" )
#        print "databases", client.database_names()
#
#    db         = client.vcf
#
#    print "databases"  , client.database_names()
#    print "collections", db.collection_names()
#
#    try:
#        db.create_collection( 'files'  )
#    except errors.CollectionInvalid:
#        pass
#
#    try:
#        db.create_collection( 'coords' )
#    except errors.CollectionInvalid:
#        pass
#
#    db.files.ensure_index(  [ ( "file_name"         , ASCENDING ) ] )
#    db.files.ensure_index(  [ ( "coll_name"         , ASCENDING ) ] )
#    db.coords.ensure_index( [ ( "CHROM"             , ASCENDING ) , ( "POS"    , ASCENDING ) ] )
#    db.coords.ensure_index( [ ( "QUAL"              , ASCENDING ) ] )
#    db.coords.ensure_index( [ ( "parent"            , ASCENDING ) ] )
#    db.coords.ensure_index( [ ( "nfo.start"         , ASCENDING ) , ( "nfo.end", ASCENDING ) ] )
#    db.coords.ensure_index( [ ( "nfo.is_deletion"   , ASCENDING ) ] )
#    db.coords.ensure_index( [ ( "nfo.is_indel"      , ASCENDING ) ] )
#    db.coords.ensure_index( [ ( "nfo.is_monomorphic", ASCENDING ) ] )
#    db.coords.ensure_index( [ ( "nfo.is_snp"        , ASCENDING ) ] )
#    db.coords.ensure_index( [ ( "nfo.is_sv"         , ASCENDING ) ] )
#    db.coords.ensure_index( [ ( "nfo.is_sv_precise" , ASCENDING ) ] )
#    db.coords.ensure_index( [ ( "nfo.is_transition" , ASCENDING ) ] )
#    db.coords.ensure_index( [ ( "nfo.sv_end"        , ASCENDING ) ] )
#
#    print "collections", db.collection_names()
#
#    mongorepr( db.files.find() , desc="DUMP RECORDS" )
#    #mongorepr( db.coords.find(), desc="DUMP RECORDS" )
#
#
#    process_file( db, sys.argv[1], os.path.basename(sys.argv[1]) )
#
#    mongorepr( db.files.find() , desc="DUMP RECORDS" )
#
#    client.close()
#
#
#
#def process_file( db, infile, iname ):
#    try:
#        files_col = db.files[ iname ]
#
#    except errors.CollectionInvalid:
#        pass
#
#    try:
#        coords_col = db.coords
#
#    except errors.CollectionInvalid:
#        pass
#
#
#    print "collections", db.collection_names()
#    print "files ", db.files.name, db.files.database, db.files.full_name
#
#    #mongorepr( db.files.find(), desc="DUMP RECORDS" )
#
#    header = 	{
#            #"_id"       : iname,
#            "file_name" : infile,
#            "coll_name" : iname,
#            "date"      : datetime.datetime.utcnow(),
#            #"nfos"      : []
#        }
#
#    vcf_reader = vcf.Reader(open(infile, 'r'))
#
#    alts       = vcf_reader.alts
#    contigs    = vcf_reader.contigs
#    filters    = vcf_reader.filters
#    formats    = vcf_reader.formats
#    infos      = vcf_reader.infos
#    meta       = vcf_reader.metadata
#
#    #print "alts   ", pp.pprint( alts    )
#    #print "contigs", pp.pprint( contigs )
#    #print "filters", pp.pprint( filters )
#    #print "formats", pp.pprint( formats )
#    #print "infos  ", pp.pprint( infos   )
#    #print "meta   ", pp.pprint( meta    )
#
#    header[ 'meta' ] = {
#        'alts'   : dict(alts   ),
#        'contigs': dict(contigs),
#        'filters': dict(filters),
#        'formats': dict(formats),
#        'infos'  : dict(infos  ),
#        'meta'   : dict(meta   )
#    }
#
#    #pp.pprint( header )
#
#    files_col.insert( header )
#
#    lastChrom    = None
#    lastChromUrl = None
#    lastPos      = None
#
#    #mongorepr( files_col.find() , desc="DUMP RECORDS files" )
#    #mongorepr( db.files.find() , desc="DUMP RECORDS files" )
#    #mongorepr( db.files[iname].find() , desc="DUMP RECORDS files" )
#    print db.files
#    print db.files[iname]
#
#    return
#
#    numrecs = 0
#    for record in vcf_reader:
#        #print pp.pprint( record )
#
#        numrecs += 1
#
#        rec = {}
#        for k in colnames:
#            val = getattr( record, k )
#            #print k, val
#            rec[ k ] = parseval( val )
#
#        chrom = rec[ 'CHROM' ]
#        pos   = rec[ 'POS'   ]
#
#
#        #http://pyvcf.readthedocs.org/en/latest/API.html#vcf-model-record
#        nfo = {
#            'aaf'           : record.aaf,            # A list of allele frequencies of alternate alleles. NOTE: Denominator calc?ed from _called_ genotypes.
#            'alleles'       : parseval( record.alleles ),        # list of alleles. [0] = REF, [1:] = ALTS
#            'call_rate'     : record.call_rate,      # The fraction of genotypes that were actually called.
#            'end'           : record.end,            # 1-based end coordinate
#            #'genotype': genotype("name")           # Lookup a _Call for the sample given in name
#            #get_hets: get_hets()                   # The list of het genotypes
#            #get_hom_alts: get_hom_alts()           # The list of hom alt genotypes
#            #get_hom_refs: get_hom_refs()           # The list of hom ref genotypes
#            #get_unknowns: get_unknowns()           # The list of unknown genotypes
#            'heterozygosity': record.heterozygosity, # Heterozygosity of a site. Heterozygosity gives the probability that two randomly chosen chromosomes from the population have different alleles, giving a measure of the degree of polymorphism in a population. If there are i alleles with frequency p_i, H=1-sum_i(p_i^2)
#            'is_deletion'   : record.is_deletion,    # Return whether or not the INDEL is a deletion
#            'is_indel'      : record.is_indel,       # Return whether or not the variant is an INDEL
#            'is_monomorphic': record.is_monomorphic, # Return True for reference calls
#            'is_snp'        : record.is_snp,         # Return whether or not the variant is a SNP
#            'is_sv'         : record.is_sv,          # Return whether or not the variant is a structural variant
#            'is_sv_precise' : record.is_sv_precise,  # Return whether the SV cordinates are mapped to 1 b.p. resolution.
#            'is_transition' : record.is_transition,  # Return whether or not the SNP is a transition
#            'nucl_diversity': record.nucl_diversity, # pi_hat (estimation of nucleotide diversity) for the site. This metric can be summed across multiple sites to compute regional nucleotide diversity estimates. For example, pi_hat for all variants in a given gene.
#            'num_called'    : record.num_called,     # The number of called samples
#            'num_het'       : record.num_het,        # The number of heterozygous genotypes
#            'num_hom_alt'   : record.num_hom_alt,    # The number of homozygous for alt allele genotypes
#            'num_hom_ref'   : record.num_hom_ref,    # The number of homozygous for ref allele genotypes
#            'num_unknown'   : record.num_unknown,    # The number of unknown genotypes
#            #'samples'       : record.samples,        # list of _Calls for each sample ordered as in source VCF
#            'start'         : record.start,          # 0-based start coordinate
#            'sv_end'        : record.sv_end,         # Return the end position for the SV
#            'var_subtype'   : record.var_subtype,    # Return the subtype of variant. - For SNPs and INDELs, yeild one of: [ts, tv, ins, del] - For SVs yield either ?complex? or the SV type defined in the ALT fields (removing the brackets). E.g.:
#            'var_type'      : record.var_type        # Return the type of variant [snp, indel, unknown] TO DO: support SVs
#        }
#
#        get_hets     = record.get_hets()
#        get_hom_alts = record.get_hom_alts()
#        get_hom_refs = record.get_hom_refs()
#        get_unknowns = record.get_unknowns()
#        samples      = record.samples
#
#        nfo[ 'get_hets'     ] = sample2dict( get_hets     )
#        nfo[ 'get_hom_alts' ] = sample2dict( get_hom_alts )
#        nfo[ 'get_hom_refs' ] = sample2dict( get_hom_refs )
#        nfo[ 'get_unknowns' ] = sample2dict( get_unknowns )
#        nfo[ 'samples'      ] = sample2dict( samples      )
#
#        rec[ 'nfo'          ] = nfo
#
#        rec[ 'parent'       ] = iname
#
#
#
#        if lastChrom != chrom:
#            lastChrom    = chrom
#            print "\n", chrom
#
#        coords_col.insert( rec )
#
#
#        if numrecs % 100 == 0:
#            print pos,"",


        #if numrecs == 100:
        #    print "DUMPING"
        #    mongorepr( coords_col.find(), desc="DUMP ALL" )
        #    mongorepr( files_col.find() , desc="DUMP ALL" )
        #    break


class Post(couchdbkit.Document):
        author = couchdbkit.StringProperty()
        content = couchdbkit.StringProperty()



def parseval( val ):
    if repr(type(val)) == "<type 'list'>":
        res = []
        for e in val:
            #print e, type(e)
            res.append( str(e) )
        return res
    else:
        return val

def mongorepr( res, desc="" ):
    if desc != "":
        print desc,
    pp.pprint( json.loads( res ) )


def sample2dict( samples ):
    #print samples

    res = []
    for sample in samples:
        re = {
            'called'    : sample.called,     # True if the GT is not ./.
            'gt_alleles': sample.gt_alleles, # The numbers of the alleles called at a given sample
            'gt_bases'  : sample.gt_bases,   # The actual genotype alleles. E.g. if VCF genotype is 0/1, return A/G
            'gt_type'   : sample.gt_type,    # The type of genotype. hom_ref = 0 het = 1 hom_alt = 2 (we don;t track _which+ ALT) uncalled = None
            'is_het'    : sample.is_het,     # Return True for heterozygous calls
            'is_variant': sample.is_variant, # Return True if not a reference call
            'phased'    : sample.phased,     # A boolean indicating whether or not the genotype is phased for this sample
            'sample'    : sample.sample,     # The sample name
            #'site'      : sample.site,       # The _Record for this _Call
        }

        re['data'] = calldata2dict( sample.data ) # Dictionary of data from the VCF file

        res.append( re )

    return res

def calldata2dict( data ):
    res = {}

    for field in data._fields:
        res[ field ] = getattr(data, field)

    return res








if __name__ == "__main__":
    main()
